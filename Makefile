CC = gcc
CFLAGS = -Wall --std=c99
server: server.o index.o

server.o: page.h index.h

index.o: page.h index.h

clean:
	rm *.o server
