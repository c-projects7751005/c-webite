#include "page.h"
#include <string.h>

void home(PAGE *p)
{
	char arrayTest[][255] = {"<li>item1</li>", "<li>item2</li>", "<li>item3</li>"};
	char page[255] = "";
	strcat(page, "<h1>C Website</h1>");
	strcat(page, "<ul>");
	for(int i = 0; i < 3; i++){
		strcat(page, arrayTest[i]);
	}
	strcat(page, "</ul>");
	p->page = page;
	p->len = strlen(p->page);
}
