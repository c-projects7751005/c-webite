#include <netinet/in.h> //structure for storing address information 
#include <stdio.h> 
#include <string.h> 
#include <stdlib.h> 
#include <sys/socket.h> //for socket APIs 
#include <sys/types.h> 

#include "page.h"
#include "index.h"

int main(int argc, char const* argv[]) 
{ 

	// create server socket similar to what was done in 
	// client program 
	int servSockD = socket(AF_INET, SOCK_STREAM, 0); 

	char postMsg[255];

	// string store data to send to client 
	char preMsg[255] = "HTTP/1.1 200 OK\n"
	"Date: Thu, 19 Feb 2009 12:27:04 GMT\n"
	"Server: Apache/2.2.3\n"
	"Last-Modified: Wed, 18 Jun 2003 16:05:58 GMT\n"
	"ETag: \"56d-9989200-1132c580\"\n"
	"Content-Type: text/html\n"
	//"Content-Length: 15\n" // needed for surf browser
	"Accept-Ranges: bytes\n"
	//"Connection: close\n"
	"\n";
	//"<h1>scott</h1>"; 

	PAGE p;

	//home(&p);

	//strcat(postMsg, preMsg);
	//strcat(postMsg, p.page);

	FILE *fptr;

	fptr = fopen("index.html", "r");

	strcat(postMsg, preMsg);
	fgets(&postMsg[strlen(preMsg)], 100, fptr);

	// define server address 
	struct sockaddr_in servAddr; 

	servAddr.sin_family = AF_INET; 
	servAddr.sin_port = htons(8082); 
	servAddr.sin_addr.s_addr = INADDR_ANY; 

	// bind socket to the specified IP and port 
	(void)bind(servSockD, (struct sockaddr*)&servAddr, 
		sizeof(servAddr)); 

	// listen for connections 
	listen(servSockD, 1); 

	// integer to hold client socket. 
	int clientSocket = accept(servSockD, NULL, NULL); 

	// send's messages to client socket 
	send(clientSocket, postMsg, sizeof(postMsg), 0);

	return 0; 
}

